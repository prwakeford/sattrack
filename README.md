# README #

A program for plotting the live position of satellites

![Alt text](example.png)

### Requirements ###

Uses the Skyfield package for SGP4 orbit perturbation algorithms, and the Basemap package for nice world projection plots


~~~~
conda install -c conda-forge skyfield
conda install -c conda-forge matplotlib
conda install -c anaconda basemap 
~~~~
### Adding / removing satellites

Satellite two-line element data is retrieved from CelesTrak. 

https://celestrak.com/NORAD/elements/

Satellites from the Science, and Stations list can be easily added to the plots. Default shows RHESSI and Hubble (HST) from the science list, and ISS from Stations.

https://celestrak.com/NORAD/elements/stations.txt
https://celestrak.com/NORAD/elements/science.txt

Modify the PlotWorld function for plotting satellites. Below line adds the excellent RHESSI satellite from the 'science' catalogue, plotted in blue.

```map, ax = overlaySatellite(map, ax, 'RHESSI','science','b')```

### To run ###
~~~~
python sattrack.py
~~~~
import numpy as np
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib.pyplot import figure
from datetime import datetime
from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
from skyfield.api import Topos, load
import time
import matplotlib.image as mpimg


# def getStationSatelliteData(satname):
#     stations_url = 'http://celestrak.com/NORAD/elements/stations.txt'
#     satellites = load.tle_file(stations_url)
#     print('Loaded', len(satellites), 'satellites')
#     by_name = {sat.name: sat for sat in satellites}
#     satellite = by_name[satname]
#     print(satellite)
#     return satellite
#
# def getScienceSatelliteData(satname):
#     science_url = 'https://www.celestrak.com/NORAD/elements/science.txt'
#     satellites = load.tle_file(science_url)
#     print('Loaded', len(satellites), 'satellites')
#     by_name = {sat.name: sat for sat in satellites}
#     satellite = by_name[satname]
#     print(satellite)
#     return satellite

def getSatelliteData(satname,catalogue):
    url = 'https://www.celestrak.com/NORAD/elements/'+catalogue+'.txt'
    satellites = load.tle_file(url)
    print('Loaded', len(satellites), 'satellites')
    by_name = {sat.name: sat for sat in satellites}
    satellite = by_name[satname]
    print(satellite)
    return satellite


def getLatLongfor90mins(satellite):
    dtn = datetime.utcnow()

    ts = load.timescale()
    timeVector = ts.utc(dtn.year, dtn.month, dtn.day, dtn.hour, range(dtn.minute, dtn.minute+90, 1))
    gc = satellite.at(timeVector)
    subpoint = gc.subpoint()
    print('----------------------')
    print(dtn)
    print(satellite)
    print('Latitude:', subpoint.latitude.degrees[0])
    print('Longitude:', subpoint.longitude.degrees[0])
    print('Elevation (m):', int(subpoint.elevation.m[0]))
    return subpoint


# Load Icons
satellite1 = mpimg.imread('satellite.png')
satellite2 = mpimg.imread('space.png')
spacestation = mpimg.imread('space-station.png')


def overlaySatellite(map, ax, SAT,CAT,col):
    SS = getSatelliteData(SAT,CAT)
    SSsubpoint = getLatLongfor90mins(SS)
    SSx,SSy = map(SSsubpoint.longitude.degrees,SSsubpoint.latitude.degrees)
    map.scatter(SSx,SSy,100,marker='.',color=col)

    if SAT == 'ISS (ZARYA)':
        im = OffsetImage(spacestation, zoom=10/ax.figure.dpi)
    else:
        im = OffsetImage(satellite2, zoom=7/ax.figure.dpi)

    im.image.axes = ax
    ab = AnnotationBbox(im, (SSx[0],SSy[0]), frameon=False, pad=0.0,)
    plt.text(SSx[0],SSy[0]-20,SAT)
    ax.add_artist(ab)
    return map, ax

def plotWorld():

    fig, ax = plt.subplots(figsize=(18, 16), dpi= 80, facecolor='w', edgecolor='k')

    pltt = True
    while pltt==True:

        map = Basemap(projection='cyl',lon_0=0)
        # plot coastlines, draw label meridians and parallels.
        map.drawcoastlines()
        map.drawparallels(np.arange(-90,90,30),labels=[1,0,0,0])
        map.drawmeridians(np.arange(map.lonmin,map.lonmax+30,60),labels=[0,0,0,1])
        # fill continents 'coral' (with zorder=0), color wet areas 'aqua'
        map.drawmapboundary(fill_color='white')

        date = datetime.utcnow()
        CS=map.nightshade(date)

        map, ax = overlaySatellite(map, ax, 'ISS (ZARYA)','stations','r')
        map, ax = overlaySatellite(map, ax, 'RHESSI','science','b')
        map, ax = overlaySatellite(map, ax, 'HST','science','b')


        plt.title('Satellite Map for %s (UTC)' % date.strftime("%d %b %Y %H:%M:%S"))
        plt.ion()
        plt.draw()
        plt.pause(60)
        plt.cla()
        #pltt=False


def runLoop():
    plotWorld()




if __name__ == "__main__":
    runLoop()
